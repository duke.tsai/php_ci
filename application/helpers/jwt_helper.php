<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function validate_token($token)
{
    $CI =& get_instance();
    $CI->load->helper('encrypt');
    $token_data = aes_decrypt(url_safe_b64_decode($token));
    $token = j_decode($token_data);
    
    return $token;
}


function validate_token_time($token)
{
    $CI =& get_instance();
    $CI->load->helper('encrypt');
    $token_data = aes_decrypt(url_safe_b64_decode($token));
    $token = j_decode($token_data);
    
    if (false !== $token && time() - $token['timestamp'] < $CI->config->item('sess_expiration'))
    {
        return $token;
    }
    
    return false;
}


