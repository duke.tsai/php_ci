<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function curl($url)
{
    $curlobj = curl_init();     
    curl_setopt($curlobj, CURLOPT_URL, $url);  
    curl_setopt($curlobj, CURLOPT_RETURNTRANSFER, 1);  
    $output=curl_exec($curlobj);  // 執行
    curl_close($curlobj); 
    return $output;
}


function get_tag_data($html, $tag, $attr, $value)
{
    $regex = "/<$tag.*?$attr=\".*?$value.*?\".*?>(.*?)<\/$tag>.*?/s";
    preg_match_all($regex,$html,$matches);
    return $matches[1];
}


function get_ip()
{
    $ipAry = array
                (
                    'HTTP_CLIENT_IP',
                    'HTTP_X_FORWARDED_FOR',
                    'HTTP_X_FORWARDED',
                    'HTTP_X_CLUSTER_CLIENT_IP',
                    'HTTP_FORWARDED_FOR',
                    'HTTP_FORWARDED',
                    'REMOTE_ADDR'
                );
                
    foreach ($ipAry as $key)
    {
        if (array_key_exists($key, $_SERVER))
        {
            foreach (explode(',', $_SERVER[$key]) as $ip)
            {
                $ip = trim($ip);
                return $ip;
            }
        }
    }
    return null;
}

