<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function aes_encrypt($message)
{
    $CI =& get_instance();
    $key = 'admin';
    $iv = $CI->config->item('encryption_key');
    
    return openssl_encrypt($message, 'AES-256-CBC', $key, 0, $iv);
}


function aes_decrypt($message)
{
    $CI =& get_instance();
    $key = 'admin';
    $iv = $CI->config->item('encryption_key');
    
    return openssl_decrypt($message, 'AES-256-CBC', $key, 0, $iv);
}


function j_decode($input)
{
    $ary = json_decode($input, true);
    
    if($ary === null){
        return false;
    }
    
    return $ary;
}


function j_encode($input)
{
    $json = json_encode($input);
    
    if($json === null){
        return false;
    }
    
    return $json;
}


function url_safe_b64_decode($input)
{
    return urldecode(base64_decode(urldecode($input)));
}


function url_safe_b64_encode($input)
{
    return urldecode(base64_encode(urlencode($input)));
}

