<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function generate()
{
    $width = 180;
    $high = 36;
    $nums = 6;
    
    $str = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMPQRSTUBWXYZ";
    $code = '';
    
    for ($i = 0; $i < $nums; $i++) 
    {
        $code .= $str[mt_rand(0, strlen($str)-1)];
    }

    $_SESSION['captcha'] = $code;
    
    header("Content-type: image/PNG");
    
    //建立圖示，設置寬度及高度與顏色等等條件
    $image = imagecreate($width, $high);
    $black = imagecolorallocate($image, mt_rand(0, 200), mt_rand(0, 200), mt_rand(0, 200));
    $border_color = imagecolorallocate($image, 21, 106, 235);
    $background_color = imagecolorallocate($image, 235, 236, 237);

    //建立圖示背景
    imagefilledrectangle($image, 0, 0, $width, $high, $background_color);

    //建立圖示邊框
    imagerectangle($image, 0, 0, $width-1, $high-1, $border_color);

    //在圖示布上隨機產生大量躁點
    for ($i = 0; $i < ($width/2); $i++) 
    {
        imagesetpixel($image, rand(0, $width), rand(0, $high), $black);
    }
   
    $strx = rand(10, 20);
    $strp = ceil(($width-10)/$nums);
    
    for ($i = 0; $i < $nums; $i++) 
    {
        $strpos = rand(1, 10);
        imagestring($image, 5, $strx, $strpos, substr($code, $i, 1), $black);
        $strx += rand($strp-5, $strp+5);
    }

    imagepng($image);
    imagedestroy($image);
}



