<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('captcha');
        $this->load->library('session');
        $this->load->model('member_model');
    }
    
    
	public function login()
	{
        $this->form_validation->set_rules('account', 'account', 'trim|required|regex_match[/^[0-9a-zA-Z]+$/]');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('captcha', 'captcha', 'trim|required|regex_match[/^[0-9a-zA-Z]+$/]');
        if ($this->form_validation->run() == FALSE)
        {
            $this->out_data(null, 'E001');
        }
        
        if ($this->post['captcha'] != $_SESSION['captcha'])
        {
            $this->out_data(null, 'E003');
        }
        
        $query_ary = array('account' => $this->post['account'], 'password' => aes_encrypt($this->post['password']));
		$member = $this->member_model->get_one($query_ary);
        
		if (empty($member))
        {
            $this->out_data(null, 'E004');
        }
            
        $token_data  = array();
        $token_data['timestamp'] = time();          
        $message = json_encode($token_data);
        $token = url_safe_b64_encode(aes_encrypt($message));

        $member_info = array(
                            'id' => $member['id'],
                            'parentId' => explode(',', $member['parentId']),
                            'account' => $member['account'],
                            'nickname' => $member['nickname'],
                            'creditQuota' => $member['creditQuota'],
                            'lottery' => $member['lottery']
                        );
        
        if (!empty($_SESSION['token']))
        {
            $session_check = array_column($_SESSION['token'], 'id');
            $token_key = array_keys($_SESSION['token']);
            
            if (in_array($member_info['id'], $session_check))
            {
                $key = array_search($member_info['id'], $session_check);
                unset($_SESSION['token'][$token_key[$key]]);
            }
        }
        $_SESSION['captcha'] = null;
        
        $_SESSION['token'][$token] = $member_info;
        $this->out_data(array('token' => $token));      
	}
    
    
    public function change_pw()
	{
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('new_pw', 'new_pw', 'trim|required');
        $this->form_validation->set_rules('confirm_pw', 'confirm_pw', 'trim|required|matches[new_pw]');
        if ($this->form_validation->run() == FALSE)
        {
            $this->out_data(null, 'E001');
        }
        
        $query_ary = array('account' => $this->member_info['account'], 'password' => aes_encrypt($this->post['password']));
		$member = $this->member_model->get_one($query_ary);
        if (empty($member))
        {
            $this->out_data(null, 'E005');
        }
        
        $update_ary = array(
                        'id' => $this->member_info['id'], 
                        'account' => $this->member_info['account'], 
                        'password' => aes_encrypt($this->post['new_pw'])
                    );
                    
        $result = $this->member_model->update_data($update_ary);
        if (!$result)
        {
            $this->out_data(null, 'E005');
        }
        $this->out_data(array('message' => '修改成功'));
    }
    
    
    public function generate_captcha()
    {
        generate();
    }
    
    
    public function logout()
	{
        unset($_SESSION['token'][$this->token]);
        $this->out_data(array('message' => '登出成功'));
    }
}
