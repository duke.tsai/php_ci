<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_api_log_model extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
        $this->table = 'frontend_api_log';
    }


    public function get_list($data, $all=true)
    {
		if(!empty($data['id'])){
			$this->db->where('id', $data['id']);
		}
        if(!empty($data['url'])){
			$this->db->where('url', $data['url']);
		}
		if(!$all){
			$this->db->limit($data['limit'], ($data['page']-1)*$data['limit']);
		}
		
        $result = $this->db->get($this->table)->result_array();
        return $result;
    }
	
	
	public function insert_data($data)
    {
		$this->db->set($data);
        $result = $this->db->insert($this->table);
        return $result;
    }
}