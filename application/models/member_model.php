<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
        $this->table = 'member';
    }


    public function get_one($data)
    {
        $this->db->where('account', $data['account']);
        $this->db->where('password', $data['password']);
        $result = $this->db->get($this->table)->row_array();
        return $result;
    }
	
	
	public function insert_data($data)
    {
		$this->db->set($data);
        $result = $this->db->insert($this->table);
        return $result;
    }
	
	
	public function update_data($data)
    {
		$this->db->set($data);
		$this->db->set('updateTime', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
        $result = $this->db->update($this->table);
        return $result;
    }
	
	
	public function delete_data($data)
    {
		$this->db->where('id', $data['id']);
        $result = $this->db->delete($this->table);
        return $result;
    }
}