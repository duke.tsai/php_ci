<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $post = null;
    public $token = null;
    public $now = null;
    public $post_data = null;
    public $member_info = null;
    
	function __construct()
	{
		parent::__construct();
        $this->load->helper('jwt');
        $this->load->helper('encrypt');
		$this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->library('session');
        
        if (isset($_POST['data']))
        {
            $this->post_data = explode('.', $_POST['data']);
            if (count($this->post_data) !== 2)
            {
                $this->out_data(null, 'E001');
            }
            $this->post = j_decode(url_safe_b64_decode($this->post_data[1]));
            if(false === $this->post){
                $this->out_data(null, 'E001');
            }
            $this->form_validation->set_data($this->post);
        }
        
        $this->validate_jwt();
        $this->now = date('Y-m-d H:i:s');
	}
    
    
    public function validate_jwt()
	{
        if (!$this->validate_pass())
        {
            if (empty($this->post_data[0]) || false === validate_token($this->post_data[0]) || empty($_SESSION['token'][$this->post_data[0]]))
            {
                $this->out_data(null, 'E002');
            }
            $this->token = $this->post_data[0];
            $this->memberInfo = $_SESSION['token'][$this->post_data[0]];
        }
	}
    
    
    public function validate_pass()
	{
        // function => controller
        $passUrl = array
                    (
                        'login' => 'login',
                        'generate_captcha' => 'login'
                    );
        
        if (isset($_SERVER['REQUEST_URI']))
        {    
            $pass = explode('/', $_SERVER['REQUEST_URI']);
        }
        else 
        {
            $pass = $_SERVER['argv'];
        }
        
        if (isset($passUrl[$pass[2]]) && $passUrl[$pass[2]] == $pass[1])
        {
            return true;
        }

        return false;
	}
    
    
    public function out_data($data, $errorCode=null)
	{
        $this->load->model('frontend_api_log_model');
        $language = (!empty($this->post) && !empty($this->post['lang'])) ? $this->post['lang'] : $this->config->item('language');
        $this->lang->load('msg', $language);
        $out_data = array
                    (
                        'error_code' => empty($error_code) ? '' : $error_code,
                        'error_msg' => empty($error_code) ? '' : $this->lang->line($errorCode),
                        'data' => empty($error_code) ? (empty($data) ? (object)[] : $data) : (object)[] 
                    );
        $out = j_encode($out_data);
        /*
        $set_log = array
                    (
                        'url' => $_SERVER['REQUEST_URI'],
                        'requestData' => empty($this->post) ? '{}' : j_encode($this->post),
                        'responseData' => $out
                    );
        $this->frontend_api_log_model->insert_data($set_log); 
        */
        header('Content-Type: application/json; charset=utf-8');
        header('Content-Length: ' . strlen($out));
        exit($out);
	}
}
